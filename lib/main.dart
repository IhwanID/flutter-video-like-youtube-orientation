import 'dart:async';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:orientation/orientation.dart';
import 'package:video_player/video_player.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: VideoTestPage(),
    );
  }
}

class VideoTestPage extends StatefulWidget {
  @override
  _VideoTestPageState createState() => _VideoTestPageState();
}

class _VideoTestPageState extends State<VideoTestPage> {
  TargetPlatform _platform;
  VideoPlayerController _videoPlayerController1;
  ChewieController _chewieController;

  ChewieFullscreenToggler _toggler;

  @override
  void initState() {
    super.initState();

    _videoPlayerController1 = VideoPlayerController.network(
        'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4');

    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController1,
      aspectRatio: 16 / 9,
      autoPlay: true,
      looping: true,
      deviceOrientationsAfterFullScreen: [],
      routePageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondAnimation, provider) {
        return AnimatedBuilder(
          animation: animation,
          builder: (BuildContext context, Widget child) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              body: Container(
                alignment: Alignment.center,
                color: Colors.black,
                child: provider,
              ),
            );
          },
        );
      },
    );

    _toggler = ChewieFullscreenToggler(_chewieController);
    WidgetsBinding.instance.addObserver(_toggler);

    _chewieController.addListener(() {
      if (_chewieController.isFullScreen && isPortrait) {
        scheduleMicrotask(() {
          OrientationPlugin.forceOrientation(DeviceOrientation.landscapeRight);
        });
      } else if (!_chewieController.isFullScreen && !isPortrait) {
        scheduleMicrotask(() {
          OrientationPlugin.forceOrientation(DeviceOrientation.portraitUp);
        });
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(_toggler);
    _videoPlayerController1.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Text",
        theme: ThemeData.light().copyWith(
          platform: _platform ?? Theme.of(context).platform,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text("sjjs"),
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: Center(
                  child: Chewie(
                    controller: _chewieController,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

class ChewieFullscreenToggler extends WidgetsBindingObserver {
  ChewieFullscreenToggler(this.chewieController)
      : assert(chewieController != null);

  final ChewieController chewieController;

  var _wasPortrait = false;

  @override
  void didChangeMetrics() {
    var _isPortrait = isPortrait;
    if (_isPortrait == _wasPortrait) {
      return;
    }

    _wasPortrait = _isPortrait;
    if (!_isPortrait && !chewieController.isFullScreen) {
      chewieController.enterFullScreen();
    } else if (_isPortrait && chewieController.isFullScreen) {
      chewieController.exitFullScreen();
    }
  }
}

bool get isPortrait {
  var size = WidgetsBinding.instance.window.physicalSize;
  return size.width < size.height;
}
